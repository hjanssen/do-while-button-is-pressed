#  Copyright (c) 2021 Henning Janssen
#

import sys

from PyQt5 import QtWidgets
from gui.up_counter import Ui_MainWindow


class UpCounter(Ui_MainWindow):
    def __init__(self) -> None:
        super().__init__()

        self.counter: int = 0

    def setup_ui(self, gui: QtWidgets.QMainWindow):
        self.setupUi(gui)

        self._reset_counter()

        # Count
        self.pushButton.pressed.connect(self._count_up)
        # Reset the counter
        self.pushButton_2.clicked.connect(lambda: self._reset_counter())

    def _reset_counter(self):
        self.counter = 0
        self.label.setText(f"{self.counter}")

    def _count_up(self):
        self.counter += 1
        self.label.setText(f"{self.counter}")


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    main_window = QtWidgets.QMainWindow()
    UpCounter().setup_ui(main_window)
    main_window.show()
    sys.exit(app.exec())
